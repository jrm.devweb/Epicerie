-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : jeu. 19 jan. 2023 à 13:36
-- Version du serveur : 8.0.21
-- Version de PHP : 8.0.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `epicerie`
--

-- --------------------------------------------------------

--
-- Structure de la table `product`
--

DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
                                       `id` int NOT NULL AUTO_INCREMENT,
                                       `name` varchar(255) NOT NULL,
  `origin` varchar(255) NOT NULL,
  `number` int NOT NULL,
  `id_photo` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `REL_bb0f00c25e50c54b75e957fe76` (`id_photo`)
  ) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `product`
--

INSERT INTO `product` (`id`, `name`, `origin`, `number`, `id_photo`) VALUES
                                                                       (1, 'Aubergine', 'Mozambique', 76, NULL),
                                                                       (2, 'Broccoli', 'Russia', 30, NULL),
                                                                       (3, 'Beetroot', 'Cameroon', 77, NULL),
                                                                       (4, 'Carrots', 'Poland', 77, NULL),
                                                                       (5, 'Courgette', 'Peru', 86, NULL),
                                                                       (6, 'Endive', 'Thailand', 75, NULL),
                                                                       (7, 'Haricot Beans', 'Pakistan', 67, NULL),
                                                                       (8, 'Iceberg Lettuce', 'Philippines', 85, NULL),
                                                                       (9, 'Red Onion', 'Somalia', 35, NULL),
                                                                       (10, 'Lentil', 'Canada', 38, NULL),
                                                                       (11, 'Potatoes', 'China', 91, NULL),
                                                                       (12, 'Olive', 'China', 18, NULL),
                                                                       (13, 'Peanuts', 'Japan', 85, NULL),
                                                                       (14, 'Rhuabarb', 'China', 81, NULL),
                                                                       (15, 'Red Potatoes', 'Kenya', 50, NULL),
                                                                       (16, 'Roma Tomatoes', 'Philippines', 89, NULL),
                                                                       (17, 'Romanesco', 'China', 52, NULL),
                                                                       (18, 'Wild Celery', 'Argentina', 36, NULL),
                                                                       (19, 'White Asparagus', 'China', 45, NULL),
                                                                       (20, 'Yellow Pepper', 'Venezuela', 23, NULL),
                                                                       (21, '', '', 0, NULL);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `FK_bb0f00c25e50c54b75e957fe760` FOREIGN KEY (`id_photo`) REFERENCES `photo` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
