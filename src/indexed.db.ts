import Dexie, {Table} from "dexie";
import {IProduct} from "../shared/Interface";

export class IndexedDb extends Dexie {
  product!: Table<IProduct, number>; // Type du contenu, Type de la PrimaryKey
  constructor() {
    super('dbProduct'); // On choisit le nom du Singleton
    this.version(1).stores({
      product: '++id' // On initialise l'id du 1er item de la table
    });
    this.on('populate', () => this.maSuperFonction());
  }

  async maSuperFonction() {
    await db.product.bulkAdd([
      {id: 1, name: 'Aubergine', origin: 'Mozambique', quantity: 76, idPhoto: 0},
      {id: 2, name: 'Broccoli', origin: 'Russia', quantity: 30, idPhoto: 0},
    ]);
  }
}

export const db = new IndexedDb();
