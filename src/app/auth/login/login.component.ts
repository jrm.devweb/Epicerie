import {Component, Input, OnInit} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import {ProductService} from "../../product/product.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @Input()
  userForm: FormGroup // Déclaration de myForm

  constructor(private formBuilder: FormBuilder, private authService: AuthService,private readonly productService: ProductService) {
    this.userForm = this.formBuilder.group({
    username: ['', Validators.required],
    password: ['', Validators.required]
  });

  }

  ngOnInit(): void {

  }

  login() {
    const {password} = this.userForm.value;
    const {username} = this.userForm.value;
    this.productService.login({username, password}).subscribe()
    this.authService.login(username, password)
    // Rediriger vers une page protégée ici

  }
}
