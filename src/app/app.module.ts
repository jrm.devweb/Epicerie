import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {ProductModule} from './product/product.module'
import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {ReactiveFormsModule} from "@angular/forms";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {AuthInterceptor} from "./auth.interceptor";
import {AdminModule} from "./admin/admin.module";
import {ServiceWorkerModule} from '@angular/service-worker';
import {environment} from '../environments/environment';
import {AuthService} from "./auth/auth.service";
import {FormsModule} from "@angular/forms";
import { LoginComponent } from './auth/login/login.component';
@NgModule({
  declarations: [
    LoginComponent,
    AppComponent
  ],
  imports: [
    FormsModule ,
    ProductModule,
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    AdminModule,
    AppRoutingModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      // Register the ServiceWorker as soon as the application is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000'
    })

  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    AuthService
  ],
  bootstrap: [AppComponent],

})
export class AppModule {
}
