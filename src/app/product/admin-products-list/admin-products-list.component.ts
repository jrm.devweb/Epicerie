import {Component, OnDestroy, OnInit} from '@angular/core';
import {IProduct, Product} from "../../../../shared/Interface";
import {ProductService} from "../product.service";
import {OnlineStatusService} from "src/online-status.service";
import {liveQuery, Observable} from "dexie";
import {Subscription} from 'dexie';
import {db} from "../../../indexed.db";
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-product-list',
  templateUrl: './admin-products-list.component.html',
  styleUrls: ['./admin-products-list.component.css']
})

export class AdminProductsListComponent implements OnInit, OnDestroy {
  title: string = 'La liste des produits'
  productList: Product[] = []
  currentProduct: Product | undefined
  //productSubscribe: any;
  isOnline: boolean | undefined;
  productArray: Array<Product> = [];
  productForm: FormGroup = this.fb.group({product: this.fb.array([])});
  productSubscribe!: Subscription;
  isLoaded: boolean = false;

  constructor(private fb: FormBuilder, private productService: ProductService, private onlineStatusService: OnlineStatusService) {
  }

  ngOnDestroy(): void {
    if (this.productSubscribe) {
      this.productSubscribe.unsubscribe();
    }
  }

  ngOnInit(): void {
    if (!this.productSubscribe) {
      this.productSubscribe = this.onlineStatusService.connectionChanged.subscribe(isOnline => {
        if (isOnline) {
          this.sendItemsFromIndexedDb();
          console.log('online');
        } else {
          console.log('offine');
        }
      });
    }
    this.productService.getAll().subscribe(res => {
      for (const product of res) {
        this.addProductForm(product);
      }
      this.isLoaded = true;
    });
  }

  onSelect(product: Product): void {
    this.currentProduct = product;
  }

  myProductForm() {
    return (this.productForm.get('product')! as FormArray);
  }

  addProductForm(product: IProduct | null) {
    if (product) {
      const productForm = this.fb.group({
        id: [product.id],
        name: [product.name, Validators.required],
        quantity: [product.quantity, Validators.required],
        origin: [product.origin, Validators.required]
      });
      this.myProductForm().push(productForm);
    } else {
      const productForm = this.fb.group({
        id: [null],
        name: ["", Validators.required],
        quantity: [null, Validators.required],
        origin: ["", Validators.required]
      });
      if (this.productForm.valid)
        this.myProductForm().push(productForm)

    }
  }

  deleteProductForm(productIndex: number) {
    let array = this.myProductForm().value
    for (let i = 0; i < array.length; i++) {
      if (array[i].id === productIndex)
        this.myProductForm().removeAt(i);
    }
    this.productService.delete(productIndex).subscribe(() => console.log('deleted'))
  }

  async listAllProducts(): Promise<Array<IProduct>> {
    return await db.product.where({}).toArray();
  }

  async addItemToIndexedDb(product: IProduct) {
    await db.product.add({...product});
  }

  handleSaveAll() {
    if (this.productForm.valid)
      this.myProductForm().value.forEach((product: IProduct) => {
        if (!product.id)
          this.productService.add(product).subscribe(() => console.log('created'))

        this.productService.update(product).subscribe(() => console.log('updated'))
      })
  }

  private async sendItemsFromIndexedDb() {
    const allItems: IProduct[] = await db.product.toArray();
    allItems.forEach((item: IProduct) => {
      // send product to backend...
      this.productService.add(item).subscribe(() => {
        db.product.delete(item.id).then(() => {
          console.log(`item ${item.id} sent and deleted locally`);
        });
      });
    });
  }
}
