import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {ProductRoutingModule} from "./product-routing/product-routing.module";

import {ProductComponent} from './product.component';
import {ProductListComponent} from './product-list/product-list.component';
import {ProductsDetailComponent} from './products-detail/products-detail.component';
import {ProductService} from "./product.service";
import {HttpClientModule} from "@angular/common/http";
import {AdminProductsListComponent} from "./admin-products-list/admin-products-list.component";

@NgModule({
  declarations: [
    ProductComponent,
    ProductListComponent,
    ProductsDetailComponent,
    AdminProductsListComponent,

  ],
  providers: [ProductService],

  imports: [
    RouterModule,
    CommonModule,
    ReactiveFormsModule,
    ProductRoutingModule,
    HttpClientModule
  ]
})
export class ProductModule {
}
