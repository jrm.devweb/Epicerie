import {Component, OnInit, Input, EventEmitter, Output} from '@angular/core';
import {IProduct} from "../../../../shared/Interface";
import {ActivatedRoute} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ProductService} from "../product.service";

@Component({
  selector: 'app-products-detail',
  templateUrl: './products-detail.component.html',
  styleUrls: ['./products-detail.component.css']
})
export class ProductsDetailComponent implements OnInit {
  @Input()
  product: IProduct | undefined;

  @Output()
  emitProduct: EventEmitter<IProduct> = new EventEmitter<IProduct>();
  editMode: Boolean = false
  productForm: FormGroup

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private productService: ProductService
  ) {
    this.productForm = this.fb.group({
      name: ['', Validators.required],
      quantity: [0, Validators.required],
      origin: ['', Validators.required],
    })
  }

  ngOnInit(): void {
    if (this.product === undefined) {
      this.route.params.subscribe(params => {
        if (params['id']) {
          let idProduct = params['id']
          //this.product = this.productService.getById(idProduct)
          this.productService.getById(idProduct).subscribe(result => {
            this.product = result
            this.productForm.patchValue(this.product)
          });
        }
      })
    }

  }

  handleEditMode() {
    this.editMode = !this.editMode
  }

  handleApply() {
    if (this.productForm.valid) {

      this.product = {...this.product, ...this.productForm.value}
    }
    // console.log('productUpdated', productUpdated, this.product)
    this.productService.update(this.product!).subscribe(() => console.log('update'))
    this.editMode = false
  }

  handleCancel() {
    this.editMode = false
  }


}
