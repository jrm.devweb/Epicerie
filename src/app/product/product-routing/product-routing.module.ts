import {NgModule} from '@angular/core';

import {RouterModule, Routes} from "@angular/router";
import {ProductsDetailComponent} from "../products-detail/products-detail.component";
import {ProductListComponent} from "../product-list/product-list.component";
import {LoginComponent} from "../../auth/login/login.component";
import {AdminProductsListComponent} from "../admin-products-list/admin-products-list.component";

const routes: Routes = [

  {
    path: 'product',
    children: [
      {path: 'edit/:id', component: ProductsDetailComponent},
      {path: 'list', component: ProductListComponent},
      {path: 'admin/list', component: AdminProductsListComponent},
      {path: 'login', component: LoginComponent},
      {path: '', redirectTo: 'list', pathMatch: 'full'}
      // {path: '', redirectTo: 'login', pathMatch: 'full'}
    ]
  }]


@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductRoutingModule {
}
