import {Injectable} from '@angular/core';
import {IProduct} from "../../../shared/Interface/product-interface";
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";


@Injectable({
  providedIn: 'root'
})
export class ProductService {
  //productList: { access_token: string } = this.getAll()

  constructor(private http: HttpClient) {

  }

  getById(idProduct: number) {
    //console.log(this.productList.find(e => e.id == idProduct))
    return this.http.get<IProduct>(environment.backendURL + '/product/' + idProduct);
  }

  getAll() {
    return this.http.get<IProduct[]>(environment.backendURL + '/product');
  }

  add(product: IProduct) {
    return this.http.post<IProduct>(environment.backendURL + '/product/', product)
  }

  delete(idProduct: number) {
    return this.http.delete<IProduct>(environment.backendURL + '/product/' + idProduct);

  }

  update(product: IProduct) {
    return this.http.patch<IProduct>(environment.backendURL + '/product/' + product.id, product);

  }
  login(user:any){
    console.log('user', user)
    return this.http.post<any>(environment.backendURL + '/user/', user)
  }
}
