import {Component, OnDestroy, OnInit} from '@angular/core';
import {IProduct, Product} from "../../../../shared/Interface";
import {ProductService} from "../product.service";
import {OnlineStatusService} from "src/online-status.service";
import {liveQuery, Observable} from "dexie";
import {Subscription} from 'dexie';
import {db} from "../../../indexed.db";
import {FormArray, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../auth/auth.service";

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})

export class ProductListComponent implements OnInit {
  rank: number = 0
  title: string = 'La liste des produits'
  productList: Product[] = []
  currentProduct: Product | undefined
  productSubscribe: any;
  isOnline: boolean | undefined;
  productArray: Array<Product> = [];

  constructor(private productService: ProductService, private onlineStatusService: OnlineStatusService, private authService: AuthService) {
    this.productService.getAll().subscribe(result => {
      this.productList = result
    });
  }

  ngOnDestroy(): void {
    if (this.productSubscribe) {
      this.productSubscribe.unsubscribe();
    }
  }

  ngOnInit(): void {
    // this.title = 'La liste des produits'
    this.productService.getAll().subscribe(products => {
      this.productArray = products;
    });

    if (!this.productSubscribe) {
      this.productSubscribe = this.onlineStatusService.connectionChanged.subscribe(isOnline => {
        if (isOnline) {
          this.sendItemsFromIndexedDb();
          this.isOnline = true;
        } else {
          this.isOnline = false;
        }
      });
    }
  }

  onSelect(product: Product): void {
    this.currentProduct = product;
  }

  listProduct: Observable<Array<IProduct>> = liveQuery(
    () => this.listAllProducts()
  );

  async listAllProducts(): Promise<Array<IProduct>> {
    return await db.product
      .where({})
      .toArray();
  }

  async addItem(product: IProduct) {
    await db.product.add({...product});
  }

  private async sendItemsFromIndexedDb() {
    const allItems: IProduct[] = await db.product.toArray();
    allItems.forEach((item: IProduct) => {
      // Attention, ici on add OU update (créer une méthode upsert en backend par ex)
      this.productService.add(item).subscribe(() => {
        db.product.delete(item.id).then(() => {
          console.log(`item ${item.id} sent and deleted locally`);
        });
      });
    });
  }

  renderRank(id: number): any {
    for (let i = 0; i < this.productList.length; i++) {
      if (this.productList[i].id === id)
        return i + 1
    }
  }
  isAdmin(){
    console.log('token', localStorage.getItem('id_token'))
    return localStorage.getItem('id_token')
  }
  logout(){
    this.authService.logout()
  }
}
