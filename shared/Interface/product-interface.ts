export interface IProduct {
  id: number,
  name: string,
  quantity: number,
  origin: string
  idPhoto: number
}

export class Product implements IProduct {
  id: number;
  name: string;
  quantity: number;
  origin: string;
  idPhoto: number;

  constructor(id: number, name: string, quantity: number, origin: string, idPhoto: number) {
    this.id = id;
    this.name = name;
    this.quantity = quantity;
    this.origin = origin;
    this.idPhoto = idPhoto
  }
}

