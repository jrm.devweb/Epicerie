import {IProduct} from "./product-interface";

export interface IType {
  id: number,
  name: string,
  product: IProduct
}

export class Type implements IType {

  id: number;
  name: string;
  product: IProduct

  constructor(id: number, name: string, product: IProduct) {
    this.id = id;
    this.name = name;
    this.product = product


  }

}

