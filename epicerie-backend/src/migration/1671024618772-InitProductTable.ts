import { MigrationInterface, QueryRunner } from "typeorm";

export class InitProductTable1671024618772 implements MigrationInterface {
    name = 'InitProductTable1671024618772'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE \`photo\` (\`id\` int NOT NULL AUTO_INCREMENT, \`name\` varchar(255) NOT NULL, \`description\` varchar(255) NULL, \`url\` varchar(255) NOT NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`type\` (\`id\` int NOT NULL AUTO_INCREMENT, \`name\` varchar(255) NOT NULL, \`idProduct\` int NOT NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`product\` (\`id\` int NOT NULL AUTO_INCREMENT, \`name\` varchar(255) NOT NULL, \`origin\` varchar(255) NOT NULL, \`quantity\` int NOT NULL, \`id_photo\` int NULL, UNIQUE INDEX \`REL_bb0f00c25e50c54b75e957fe76\` (\`id_photo\`), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`ALTER TABLE \`type\` ADD CONSTRAINT \`FK_35797b8f50f38d02c00d3d8c0cc\` FOREIGN KEY (\`idProduct\`) REFERENCES \`product\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`product\` ADD CONSTRAINT \`FK_bb0f00c25e50c54b75e957fe760\` FOREIGN KEY (\`id_photo\`) REFERENCES \`photo\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`product\` DROP FOREIGN KEY \`FK_bb0f00c25e50c54b75e957fe760\``);
        await queryRunner.query(`ALTER TABLE \`type\` DROP FOREIGN KEY \`FK_35797b8f50f38d02c00d3d8c0cc\``);
        await queryRunner.query(`DROP INDEX \`REL_bb0f00c25e50c54b75e957fe76\` ON \`product\``);
        await queryRunner.query(`DROP TABLE \`product\``);
        await queryRunner.query(`DROP TABLE \`type\``);
        await queryRunner.query(`DROP TABLE \`photo\``);
    }

}
