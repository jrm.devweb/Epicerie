import {MigrationInterface, QueryRunner} from "typeorm"
import {AppDataSource} from "../data-source";
import {Product} from "../product/entities/product.entity";

export class InitProductTable1671026807053 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<void> {
    await AppDataSource
      .createQueryBuilder()
      .insert()
      .into(Product)
      .values([
        {id: 1, name: "Aubergine", quantity: 76, origin: "Mozambique"},
        {id: 2, name: "Broccoli", quantity: 30, origin: "Russia"},
        {id: 3, name: "Beetroot", quantity: 77, origin: "Cameroon"},
        {id: 4, name: "Carrots", quantity: 77, origin: "Poland"},
        {id: 5, name: "Courgette", quantity: 86, origin: "Peru"},
        {id: 6, name: "Endive", quantity: 75, origin: "Thailand"},
        {id: 7, name: "Haricot Beans", quantity: 67, origin: "Pakistan"},
        {id: 8, name: "Iceberg Lettuce", quantity: 85, origin: "Philippines"},
        {id: 9, name: "Red Onion", quantity: 35, origin: "Somalia"},
        {id: 10, name: "Lentil", quantity: 38, origin: "Canada"},
        {id: 11, name: "Potatoes", quantity: 91, origin: "China"},
        {id: 12, name: "Olive", quantity: 18, origin: "China"},
        {id: 13, name: "Peanuts", quantity: 85, origin: "Japan"},
        {id: 14, name: "Rhuabarb", quantity: 81, origin: "China"},
        {id: 15, name: "Red Potatoes", quantity: 50, origin: "Kenya"},
        {id: 16, name: "Roma Tomatoes", quantity: 89, origin: "Philippines"},
        {id: 17, name: "Romanesco", quantity: 52, origin: "China"},
        {id: 18, name: "Wild Celery", quantity: 36, origin: "Argentina"},
        {id: 19, name: "White Asparagus", quantity: 45, origin: "China"},
        {id: 20, name: "Yellow Pepper", quantity: 23, origin: "Venezuela"}
      ])
      .execute()
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
  }

}
