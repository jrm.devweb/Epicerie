import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {ProductModule} from './product/product.module';
import {PhotoModule} from './photo/photo.module';
import {TypeModule} from './type/type.module';
import {dbProperties} from "./properties/dbProperties";
import {TypeOrmModule} from "@nestjs/typeorm";
import 'reflect-metadata';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [TypeOrmModule.forRoot({...dbProperties}), ProductModule, PhotoModule, TypeModule, AuthModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
}
