import {IPhoto} from "@epicerie/shared";
import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";


@Entity('photo')
export class Photo implements IPhoto {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({name: 'name', nullable: false})
  name: string;
  @Column({name: 'description', nullable: true})
  description: string;
  @Column({name: 'url', nullable: false})
  url: string;
}
