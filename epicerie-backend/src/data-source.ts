import "reflect-metadata"
import {DataSource} from "typeorm"
import {dbProperties} from "./properties/dbProperties";

export const AppDataSource = new DataSource({...dbProperties})
