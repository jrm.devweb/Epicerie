export const dbProperties: any = {
  type: "mysql",
  host: "localhost",
  port: 3306,
  username: "root",
  password: "",
  database: "epicerie",
  synchronize: true,
  logging: false,
  entities: ['src/**/**.entity{.ts,.js}'],
  migrations: ['src/migration/**'],
  subscribers: [],
}
