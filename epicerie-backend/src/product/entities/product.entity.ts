import {IProduct} from "@epicerie/shared";
import {Column, Entity, JoinColumn, OneToMany, OneToOne, PrimaryGeneratedColumn} from "typeorm";
import {Photo} from "../../photo/entities/photo.entity";
import {Type} from "../../type/entities/type.entity";

@Entity('product')
export class Product implements IProduct {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({name: 'name', nullable: false})
  name: string;
  @Column({name: 'origin'})
  origin: string;
  @Column({name: 'quantity'})
  quantity: number;

  @OneToOne(() => Photo)
  @JoinColumn({name: 'id_photo', referencedColumnName: 'id'})
  idPhoto: number;

  @OneToMany(() => Type, (t) => t.product)
  @JoinColumn()
  types: Type[];

}
