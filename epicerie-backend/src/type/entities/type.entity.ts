import {Product} from "../../product/entities/product.entity";
import {Column, Entity, PrimaryGeneratedColumn, ManyToMany, OneToMany, OneToOne, ManyToOne, JoinColumn} from "typeorm";
import {IType} from "@epicerie/shared";

@Entity('type')
export class Type implements IType {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({name: 'name', nullable: false})
  name: string;

  @ManyToOne(() => Product, (t) => t.types)
  @JoinColumn({name: 'idProduct'})
  product: Product;

  @Column()
  idProduct: number

}
